from django.shortcuts import render, redirect
from proyecto1.forms import LoginForm
from django.contrib.auth import authenticate, login, logout

def login_page(request):
    message = None
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request,user)
                    return redirect('home')
            else:
                message = 'Inactivo'
        else:
            message = 'Datos incorrectos'
    else:
        form = LoginForm()
    return render(request,'login.html',{'message':message,'form':form})

def home(request):
    return render(request,'home.html',{})

def exit(request):
    logout(request)
    return redirect('home')
