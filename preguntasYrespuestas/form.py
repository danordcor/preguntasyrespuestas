from django import forms
from preguntasYrespuestas.models import Pregunta

class PreguntaForm(forms.ModelForm):
    class Meta:
        model = Pregunta
        fields = "__all__"

class RespuestaForm(forms.Form):
    contenido = forms.CharField(widget=forms.TextInput, required=True)
